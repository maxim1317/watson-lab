FROM alpine:3.7

RUN apk --no-cache --update-cache add \
    bash \
    gcc \
    gfortran \
    python3 python3-dev py3-pip \
    build-base \
    wget \
    py3-qt5 \
    freetype-dev libpng-dev openblas-dev


RUN pip3 install --upgrade pip
RUN pip3 install numpy

RUN pip3 install praw==6.1.1 watson_developer_cloud==2.8.0 matplotlib==2.2.2

ADD app /ibm/app
ADD src/ /ibm/src/
ADD run.sh /ibm

WORKDIR /ibm

ENV TERM=xterm-256color

CMD ./run.sh