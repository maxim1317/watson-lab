from PyQt5 import QtCore, QtGui, QtWidgets

from utils.style import *
from utils.gui_utils import *
from utils.misc import load_users


class IBMWindow(object):
    def setupUi(self, IBMWindow):
        IBMWindow.setObjectName("IBMWindow")
        IBMWindow.setFixedSize(800, 500)
        center(IBMWindow)
        IBMWindow.setWindowTitle("Personality Insights")

        self.saved_users = load_users()

        font = QtGui.QFont()
        font.setFamily("Roboto")
        font.setPointSize(18)
        IBMWindow.setFont(font)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("resources/images/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        IBMWindow.setWindowIcon(icon)

        self.theme = Theme(dark=False)
        self.style = Style().style
        IBMWindow.setStyleSheet(self.style)

        self.centralwidget = QtWidgets.QWidget(IBMWindow)
        self.centralwidget.setObjectName("centralwidget")

        if True:
            self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
            self.verticalLayout_2.setObjectName("verticalLayout_2")

            if True:
                self.verticalLayout = QtWidgets.QVBoxLayout()
                self.verticalLayout.setObjectName("verticalLayout")

                if True:
                    spacerItem_6 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                    self.verticalLayout.addItem(spacerItem_6)

                    self.personalityInsightsLBL = QtWidgets.QLabel(self.centralwidget)
                    self.personalityInsightsLBL.setText("Personality Insights")
                    self.personalityInsightsLBL.setObjectName("personalityInsightsLBL")
                    self.personalityInsightsLBL.setAlignment(QtCore.Qt.AlignCenter)
                    font = QtGui.QFont()
                    font.setPointSize(24)
                    self.personalityInsightsLBL.setFont(font)
                    self.verticalLayout.addWidget(self.personalityInsightsLBL)

                    spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                    self.verticalLayout.addItem(spacerItem)

                    self.horizontalLayout = QtWidgets.QHBoxLayout()
                    self.horizontalLayout.setObjectName("horizontalLayout")

                    if True:
                        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
                        self.verticalLayout_4.setObjectName("verticalLayout_4")

                        if True:
                            self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
                            self.horizontalLayout_3.setObjectName("horizontalLayout_3")

                            if True:
                                self.usernameLE = QtWidgets.QLineEdit(self.centralwidget)
                                self.usernameLE.setText("")
                                self.usernameLE.setPlaceholderText("Username...")
                                self.usernameLE.setObjectName("usernameLE")
                                font = QtGui.QFont()
                                font.setPointSize(18)
                                self.usernameLE.setFont(font)
                                completer = QtWidgets.QCompleter(self.saved_users, self.usernameLE)
                                self.usernameLE.setCompleter(completer)        # Set QCompleter in the input field
                                self.usernameLE.setFocus()
                                self.horizontalLayout_3.addWidget(self.usernameLE)

                                self.goPB = QtWidgets.QPushButton(self.centralwidget)
                                self.goPB.setText("GO!")
                                font = QtGui.QFont()
                                font.setPointSize(18)
                                self.goPB.setFont(font)
                                self.goPB.setObjectName("goPB")

                            self.horizontalLayout_3.addWidget(self.goPB)

                        self.verticalLayout_4.addLayout(self.horizontalLayout_3)

                        self.errorLBL = QtWidgets.QLabel(self.centralwidget)
                        self.errorLBL.setText("Error: ")
                        self.errorLBL.setObjectName("errorLBL")
                        # self.errorLBL.setAlignment(QtCore.Qt.AlignCenter)
                        self.errorLBL.setStyleSheet("color: " + self.theme.red + ";")
                        font = QtGui.QFont()
                        font.setPointSize(12)
                        self.errorLBL.setFont(font)
                        self.errorLBL.setHidden(True)
                        self.verticalLayout.addWidget(self.errorLBL)

                        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                        self.verticalLayout_4.addItem(spacerItem1)

                        self.stackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
                        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
                        sizePolicy.setHorizontalStretch(0)
                        sizePolicy.setVerticalStretch(0)
                        sizePolicy.setHeightForWidth(self.stackedWidget.sizePolicy().hasHeightForWidth())
                        self.stackedWidget.setSizePolicy(sizePolicy)
                        self.stackedWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
                        self.stackedWidget.setObjectName("stackedWidget")

                        self.stackedWidget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

                        if True:
                            self.plotPG = QtWidgets.QWidget()
                            self.plotPG.setObjectName("plotPG")

                            if True:
                                self.plotVBL = QtWidgets.QVBoxLayout(self.plotPG)

                                if True:
                                    self.plotLBL = QtWidgets.QLabel(self.plotPG)
                                    self.plotLBL.setGeometry(QtCore.QRect(60, 40, 552, 45))
                                    self.plotLBL.setText("Here\'s picture")
                                    self.plotLBL.setAlignment(QtCore.Qt.AlignCenter)
                                    self.plotLBL.setObjectName("plotLBL")
                                    self.plotVBL.addWidget(self.plotLBL)

                            self.stackedWidget.addWidget(self.plotPG)

                            self.pBarPG = QtWidgets.QWidget()
                            self.pBarPG.setObjectName("pBarPG")

                            if True:
                                self.pBarVBL = QtWidgets.QVBoxLayout(self.pBarPG)

                                if True:
                                    self.loadBarDescLBL = QtWidgets.QLabel(self.plotPG)
                                    self.loadBarDescLBL.setGeometry(QtCore.QRect(60, 40, 552, 45))
                                    self.loadBarDescLBL.setText("Loading...")
                                    font = QtGui.QFont()
                                    font.setPointSize(18)
                                    self.loadBarDescLBL.setFont(font)
                                    self.loadBarDescLBL.setObjectName("loadBarDescLBL")
                                    self.pBarVBL.addWidget(self.loadBarDescLBL)

                                    self.loadBar = QtWidgets.QProgressBar(self.pBarPG)
                                    self.loadBar.setGeometry(QtCore.QRect(0, 0, 200, 50))
                                    self.loadBar.setProperty("value", 0)
                                    self.loadBar.setAlignment(QtCore.Qt.AlignCenter)
                                    self.loadBar.setObjectName("loadBar")
                                    font = QtGui.QFont()
                                    font.setPointSize(18)
                                    self.loadBar.setFont(font)
                                    self.pBarVBL.addWidget(self.loadBar)

                            self.stackedWidget.addWidget(self.pBarPG)

                        self.verticalLayout_4.addWidget(self.stackedWidget)

                        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                        self.verticalLayout_4.addItem(spacerItem2)

                    self.horizontalLayout.addLayout(self.verticalLayout_4)

                    self.verticalLayout_5 = QtWidgets.QVBoxLayout()
                    self.verticalLayout_5.setObjectName("verticalLayout_5")

                    if True:
                        self.line = QtWidgets.QFrame(self.centralwidget)
                        self.line.setFrameShape(QtWidgets.QFrame.VLine)
                        self.line.setStyleSheet("color: " + self.theme.accent + ";")
                        self.line.setObjectName("line")
                        self.verticalLayout_5.addWidget(self.line)

                    self.horizontalLayout.addLayout(self.verticalLayout_5)

                    self.frame = QtWidgets.QFrame()

                    self.verticalLayout_3 = QtWidgets.QVBoxLayout()
                    self.verticalLayout_3.setObjectName("verticalLayout_3")

                    if True:
                        self.todayYouAreLBL = QtWidgets.QLabel(self.centralwidget)
                        self.todayYouAreLBL.setText("Today you are...")
                        self.todayYouAreLBL.setObjectName("todayYouAreLBL")
                        font = QtGui.QFont()
                        font.setPointSize(21)
                        self.todayYouAreLBL.setFont(font)
                        self.verticalLayout_3.addWidget(self.todayYouAreLBL)

                        spacerItem01 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                        self.verticalLayout_3.addItem(spacerItem01)

                        self.daylyInsightsLW = QtWidgets.QListWidget(self.centralwidget)
                        self.daylyInsightsLW.setObjectName("daylyInsightsLW")
                        self.verticalLayout_3.addWidget(self.daylyInsightsLW)

                        spacerItem02 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                        self.verticalLayout_3.addItem(spacerItem02)

                        self.thanUsualLBL = QtWidgets.QLabel(self.centralwidget)
                        self.thanUsualLBL.setText("...than usual")
                        self.thanUsualLBL.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
                        self.thanUsualLBL.setObjectName("thanUsualLBL")
                        font = QtGui.QFont()
                        font.setPointSize(21)
                        self.thanUsualLBL.setFont(font)
                        self.verticalLayout_3.addWidget(self.thanUsualLBL)

                    self.frame.setLayout(self.verticalLayout_3)

                    self.horizontalLayout.addWidget(self.frame)

                self.verticalLayout.addLayout(self.horizontalLayout)

                self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
                self.horizontalLayout_2.setObjectName("horizontalLayout_2")

                spacerItem_5 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
                self.verticalLayout.addItem(spacerItem_5)

                if True:
                    spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
                    self.horizontalLayout_2.addItem(spacerItem3)

                    self.quitPB = QtWidgets.QPushButton(self.centralwidget)
                    self.quitPB.setText("QUIT")
                    self.quitPB.setObjectName("quitPB")
                    font = QtGui.QFont()
                    font.setPointSize(18)
                    self.quitPB.setFont(font)
                    self.horizontalLayout_2.addWidget(self.quitPB)

                self.verticalLayout.addLayout(self.horizontalLayout_2)

            self.verticalLayout_2.addLayout(self.verticalLayout)

        IBMWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(IBMWindow)
        self.statusbar.setObjectName("statusbar")
        IBMWindow.setStatusBar(self.statusbar)

        self.stackedWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(IBMWindow)

    def draw_compare(self, compared, mode='Big_5'):
        comparison = {
            'Big_5': {
                "Openness"              : 'open',
                "Conscientiousness"     : 'conscientious',
                "Extraversion"          : 'extraverted',
                "Agreeableness"         : 'agreeable',
                "Emotional range"       : 'emotional'
            },
            'Emotions': {
                "Fiery"                 : 'fiercest',
                "Prone to worry"        : 'prone to worry',
                "Melancholy"            : 'melancholic',
                "Immoderation"          : 'immoderate',
                "Self-consciousness"    : 'self-conscious',
                "Susceptible to stress" : 'susceptible to stress'
            }
        }

        if mode == 'Big_5':
            for key in comparison['Big_5'].keys():
                itemN        = QtWidgets.QListWidgetItem()
                widget       = QtWidgets.QWidget()

                if compared['Big_5'][key] == 'less':
                    widgetText   = QtWidgets.QLabel(
                        '<b><font color=' + self.theme.red + '>' + compared['Big_5'][key].title() + '</font></b>' +
                        ' ' + comparison['Big_5'][key]
                    )
                else:
                    widgetText   = QtWidgets.QLabel(
                        '<b><font color=' + self.theme.green + '>' + compared['Big_5'][key].title() + '</font></b>' +
                        ' ' + comparison['Big_5'][key]
                    )

                font = QtGui.QFont()
                font.setPointSize(18)
                widgetText.setFont(font)
                widgetLayout = QtWidgets.QHBoxLayout()
                widgetLayout.addWidget(widgetText)
                widgetLayout.addStretch()

                widgetLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
                widget.setLayout(widgetLayout)
                itemN.setSizeHint(widgetText.sizeHint())

                self.daylyInsightsLW.addItem(itemN)
                self.daylyInsightsLW.setItemWidget(itemN, widgetText)
