from PyQt5.QtWidgets import QApplication, QMainWindow, QSplashScreen
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt, QThread, pyqtSignal
import sys

import ibm

from utils.style import *

from utils.reddit_utils import get_reddit_data
from utils.watson_utils import get_watson_data
from utils.plot_utils import plot_pi

from utils.misc import save_user, check_user, json_to_dict


class Loader(QThread):
    # Create the signal
    sig_result   = pyqtSignal(dict)
    sig_progress = pyqtSignal(dict)

    def __init__(self, username, parent=None):
        from os.path import join

        # super(Updater, self).__init__()
        QThread.__init__(self)
        # Connect signal to the desired function
        # self.sig.connect(update)

        self.username = username.lower()

        self.redditPMax = 25
        self.watsonPMax = 90
        self.plotPMax   = 100

        self.path = join('users', self.username)

        # self.steps = {
        #     0: get_reddit_data,
        #     1: get_reddit_data,
        #     2: get_watson_data,
        #     3: get_watson_data,
        # }

    def run(self):
        from os import listdir
        from os.path import join

        try:
            cur_step = check_user(join('resources', 'saved.json'), self.username)
            print(cur_step)
            if cur_step < 2:
                reddit_paths = get_reddit_data(
                    username=self.username,
                    path=self.path,
                    prange=(0, self.redditPMax),
                    signal=self.sig_progress
                )
            else:
                reddit_paths = [join('users', self.username, 'prepared', f) for f in listdir(join('users', self.username, 'prepared'))]

            if cur_step < 4:
                watson_path, compared_path = get_watson_data(
                    path=self.path,
                    prepared_paths=reddit_paths,
                    prange=(self.redditPMax, self.watsonPMax),
                    signal=self.sig_progress
                )
            else:
                watson_path   = join('users', self.username, 'condensed', 'condensed.json')
                compared_path = join('users', self.username, 'final'    , 'compared.json')

            plot_path = plot_pi(
                path=self.path,
                analysed_path=watson_path,
                prange=(self.watsonPMax, self.plotPMax),
                signal=self.sig_progress,
                mode='Big_5'
            )

            save_user(user=self.username, path=join('resources', 'saved.json'))

            self.sig_result.emit({
                'plot'    : plot_path,
                'compared': compared_path
            })

        except Exception as error:
            if hasattr(error, 'code'):
                code = str(error.code)
                error_msg = error.message
            else:
                code = None
                error_msg = str(error)
            self.sig_progress.emit(
                {
                    'status'    : False,
                    'error_code': code,
                    'error'     : error_msg,
                    'p'         : None,
                    'desc'      : None
                }
            )
            # raise

            return


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        self.theme = Theme(dark=False)

        splash_pix = QPixmap('resources/images/ibm.png')
        self.splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint)
        self.splash.setMask(splash_pix.mask())
        self.splash.show()
        app.processEvents()

        super(MainWindow, self).__init__(parent)

        self.uiIBM = ibm.IBMWindow()

        self.startIBM()

    def startIBM(self):
        self.uiIBM.setupUi(self)

        self.uiIBM.loadBar.setHidden(True)
        self.uiIBM.loadBarDescLBL.setHidden(True)
        self.uiIBM.frame.hide()
        self.uiIBM.goPB.clicked.connect(self.start_loading)
        self.uiIBM.quitPB.clicked.connect(self.quit)

        self.show()
        self.splash.finish(self)

    def quit(self):
        self.close()

    def start_loading(self):
        self.uiIBM.stackedWidget.setCurrentIndex(1)
        self.uiIBM.loadBar.setHidden(False)
        self.uiIBM.frame.hide()
        self.uiIBM.errorLBL.setHidden(True)
        self.uiIBM.goPB.setEnabled(False)

        while self.uiIBM.daylyInsightsLW.count() > 0:
            self.uiIBM.daylyInsightsLW.takeItem(0)

        self.uiIBM.usernameLE.setStyleSheet("border-bottom: 2px solid " + self.theme.accent + ";")

        username = self.uiIBM.usernameLE.text()
        self.threadLoader = Loader(username=username)

        self.threadLoader.sig_progress.connect(self.get_progress)
        self.threadLoader.sig_result.connect(self.load_done)
        self.threadLoader.start()

    def get_progress(self, progress):
        status = progress['status']

        if status is False:
            error = ''
            if progress['error_code'] is not None:
                error += progress['error_code'] + ': '
            error += progress['error']
            self.revert_load(error=error)
        elif status is True:
            self.uiIBM.usernameLE.setStyleSheet("border-bottom: 2px solid " + self.theme.green + ";")

        self.uiIBM.loadBar.setProperty('value', progress['p'])
        self.uiIBM.loadBar.setFormat(progress['desc'])

        return

    def revert_load(self, error=None):
        self.uiIBM.usernameLE.setStyleSheet("border-bottom: 2px solid " + self.theme.red + ";")
        self.uiIBM.loadBar.setHidden(True)
        self.uiIBM.goPB.setEnabled(True)

        self.uiIBM.errorLBL.setText(error)
        self.uiIBM.errorLBL.setHidden(False)

        return

    def load_done(self, result):
        self.uiIBM.usernameLE.setStyleSheet("border-bottom: 2px solid " + self.theme.accent + ";")
        self.uiIBM.loadBar.setHidden(True)
        self.uiIBM.goPB.setEnabled(True)
        self.uiIBM.frame.show()

        plot = QPixmap(result['plot'])
        self.uiIBM.plotLBL.setPixmap(plot)
        # h = 256
        # w = (6 / 8) * h
        # smaller_plot = plot.scaled(h, w, Qt.KeepAspectRatio)
        # self.uiIBM.plotLBL.setPixmap(smaller_plot)
        self.uiIBM.stackedWidget.setCurrentIndex(0)


        self.uiIBM.draw_compare(compared=json_to_dict(result['compared']))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MainWindow()
    sys.exit(app.exec_())
