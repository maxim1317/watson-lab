def json_to_dict(json_path):
    '''
        Reads JSON file and returns a dict
    '''
    from json import loads

    with open(json_path) as raw:
        jdict = loads(raw.read())
    return jdict


def dict_to_json(jdict, json_path):
    '''
        Writes dict as JSON to file
    '''
    from json import dump

    with open(json_path, 'w') as raw:
        dump(jdict, raw, indent=4, sort_keys=False, ensure_ascii=False)
    return


def mk_dir(path):
    from os import makedirs
    from os.path import exists

    if not exists(path):
        makedirs(path)


def file_count(path):
    import os
    return len([name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))])


def save_user(path, user):
    users = json_to_dict(path)
    if user not in users:
        users.append(user)
        dict_to_json(users, path)
    return


def load_users():
    from os.path import join
    users = json_to_dict(join('resources', 'saved.json'))
    return users


def check_user(path, user):
    from os.path import join, isdir

    users = json_to_dict(path)

    # print(path)

    if user not in users:
        return False

    path  = join('users', user)

    steps = {
        0: 'raw',
        1: 'prepared',
        2: 'analysed',
        3: 'condensed',
    }

    cur_step = 0
    for i in reversed(range(4)):
        if isdir(join(path, steps[i])) and file_count(join(path, steps[i])):
            cur_step = i + 1
            break

    return cur_step
