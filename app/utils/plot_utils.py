from utils.misc import *


def plot_pi(path, analysed_path, prange, signal, mode='Big_5'):
    import matplotlib.pyplot as plt
    import matplotlib.dates as dates

    import datetime

    from os.path import join

    progress = {
        'status'     : None,
        'error'      : None,
        'error_code' : None,
        'p'          : prange[0],
        'desc'       : None
    }

    progress['desc'] = 'Plotting insights...'
    signal.emit(progress)

    output_path = join(path, 'plot')
    mk_dir(output_path)
    output_path = join(output_path, 'plot.png')

    # plt.xkcd()

    fig, ax = plt.subplots()
    # plt.figure(figsize=(w, h))

    plt.gca().xaxis.set_major_formatter(dates.DateFormatter('%Y-%m'))
    # plt.gca().xaxis.set_major_locator(dates.DayLocator())

    plt.gca().xaxis.set_major_locator(dates.MonthLocator())
    plt.gca().xaxis.set_minor_locator(dates.DayLocator())

    handles, labels = ax.get_legend_handles_labels()

    data = json_to_dict(analysed_path)

    x   = []
    y   = []
    y_1 = []
    y_2 = []
    y_3 = []
    y_4 = []
    y_5 = []
    y_6 = []

    if mode == 'Big_5':

        for date in data.keys():
            x.append(datetime.datetime.fromtimestamp(int(date)).date())
            y.append(data[date]["Big_5"])

        x, y = zip(*sorted(zip(x, y)))

        for yi in y:
            y_1.append(yi["Openness"])
            y_2.append(yi["Conscientiousness"])
            y_3.append(yi["Extraversion"])
            y_4.append(yi["Agreeableness"])
            y_5.append(yi["Emotional range"])
        # print(x, y_1)
        # x = list(x)

        l_1, = ax.plot(x, y_1, '--', linewidth=1, label="Openness")
        l_2, = ax.plot(x, y_2, '--', linewidth=1, label="Conscientiousness")
        l_3, = ax.plot(x, y_3, '--', linewidth=1, label="Extraversion")
        l_4, = ax.plot(x, y_4, '--', linewidth=1, label="Agreeableness")
        l_5, = ax.plot(x, y_5, '--', linewidth=1, label="Emotional range")

        l_1 = ax.scatter(x, y_1, 2, label="Openness")
        l_2 = ax.scatter(x, y_2, 2, label="Conscientiousness")
        l_3 = ax.scatter(x, y_3, 2, label="Extraversion")
        l_4 = ax.scatter(x, y_4, 2, label="Agreeableness")
        l_5 = ax.scatter(x, y_5, 2, label="Emotional range")

        plt.legend(handles=[l_1, l_2, l_3, l_4, l_5], loc=2)

    else:
        for date in data.keys():
            x.append(datetime.datetime.fromtimestamp(int(date)).date())
            y.append(data[date]["Emotions"])

        x, y = zip(*sorted(zip(x, y)))

        for yi in y:
            y_1.append(yi["Fiery"])
            y_2.append(yi["Prone to worry"])
            y_3.append(yi["Melancholy"])
            y_4.append(yi["Immoderation"])
            y_5.append(yi["Susceptible to stress"])
            y_6.append(yi["Self-consciousness"])

        l_1, = ax.plot(x, y_1, label="Fiery")
        l_2, = ax.plot(x, y_2, label="Prone to worry")
        l_3, = ax.plot(x, y_3, label="Melancholy")
        l_4, = ax.plot(x, y_4, label="Immoderation")
        l_5, = ax.plot(x, y_5, label="Susceptible to stress")
        l_6, = ax.plot(x, y_6, label="Self-consciousness")

        plt.legend(handles=[l_1, l_2, l_3, l_4, l_5, l_6], loc=2)

    fig.autofmt_xdate()

    # colors = ['r', 'g', 'b', 'y', 'c']
    dpi = 150

    plt.savefig(
        fname=output_path,
        dpi=dpi,
        type='png'
    )

    output_path = resize(output_path)

    progress['p'] = prange[1]
    signal.emit(progress)

    return output_path


def resize(filename):
    import cv2

    W = 500

    oriimg = cv2.imread(filename)

    height, width, depth = oriimg.shape
    imgScale = W / width

    newX, newY = oriimg.shape[1] * imgScale, oriimg.shape[0] * imgScale

    newimg = cv2.resize(oriimg, (int(newX), int(newY)), interpolation=cv2.INTER_AREA)

    cv2.imwrite(filename, newimg)

    return filename
