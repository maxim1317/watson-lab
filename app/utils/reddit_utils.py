from utils.misc import *


def reddit_auth():
    import praw

    credentials = json_to_dict('resources/credentials/cre_reddit.json')

    try:
        r = praw.Reddit(
            user_agent=credentials['my_user_agent'],
            client_id=credentials['my_client_id'],
            client_secret=credentials['my_client_secret']
        )
        sub = r.subreddit('redditdev')
        title = sub.title

        return r

    except Exception:
        raise


def check_user(reddit, username):
    user = reddit.redditor(username)

    user_comments = user.comments.new(limit=1)

    try:
        for comment in user_comments:
            return True
    except Exception:
        raise


def get_comments(reddit, username, path):
    import datetime
    import time
    from os.path import join

    mk_dir(join(path, 'raw'))

    user = reddit.redditor(username)

    user_comments = user.comments.new(limit=None)

    data = {}

    for comment in user_comments:
        base_time = datetime.datetime.min.time()
        d = datetime.datetime.combine(datetime.datetime.fromtimestamp(comment.created).date(), base_time)
        date = int(time.mktime(d.timetuple()))
        if date not in data:
            data[date] = {
                'comments': []
            }
        data[date]['comments'].append(comment.body)

    path = join(path, 'raw', username + '.json')

    dict_to_json(data, path)

    return path


def raw_processing(path, raw_path):
    """Prepare raw data to be sendable to Watson

    Args:
        path (String): path to user folder (ex. "users/georgy_k_zhukov/")
        raw_path (String): path to raw data JSON (ex. "users/georgy_k_zhukov/raw/georgy_k_zhukov.json")

    Returns:
        List: list of JSONs - data split by days and prepared for Watson. Each name is UNIX timestamp for the day
    """
    import time
    import datetime
    import os.path

    raw = json_to_dict(raw_path)

    mk_dir(os.path.join(path, 'prepared'))

    paths = []

    for date in raw.keys():
        now = datetime.datetime.now()
        wats = {
            'id'            : str(int(time.mktime(now.timetuple()))),
            'contenttype'   : 'text/plain',
            'created'       : date,
            'language'      : 'en',
            'content'         : ''
        }

        for comment in raw[date]['comments']:
            wats['content'] += comment + '.'

        output = {
            "contentItems": [wats]
        }

        out_path = os.path.join(path, 'prepared', date + '.json')

        dict_to_json(output, out_path)
        paths.append(out_path)

    return paths


def get_reddit_data(username, path, prange, signal):
    progress = {
        'status'     : None,
        'error'      : None,
        'error_code' : None,
        'p'          : prange[0],
        'desc'       : None
    }

    delta = prange[1] - prange[0]

    try:
        progress['desc'] = 'Connecting to reddit.com...'
        signal.emit(progress)
        reddit = reddit_auth()
        progress['p']    = 0.1 * delta

        progress['desc'] = 'Checking if user exists...'
        signal.emit(progress)
        check_user(reddit, username)
        progress['status'] = True
        progress['p']    = 0.2 * delta

        progress['desc'] = 'Getting user comments...'
        signal.emit(progress)
        raw_path = get_comments(reddit, username, path)
        progress['p']    = 0.9 * delta

        progress['desc'] = 'Processing user comments...'
        signal.emit(progress)
        paths = raw_processing(path, raw_path)
        progress['p']    = 1.0 * delta

        progress['desc'] = 'Done!'
        signal.emit(progress)

        return paths

    except Exception:
        raise


if __name__ == '__main__':
    reddit = reddit_auth()
    username = 'oberon1317'
    check_user(reddit, username)
