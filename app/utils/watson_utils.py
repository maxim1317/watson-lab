from utils.misc import *


def watson_auth():
    from watson_developer_cloud import PersonalityInsightsV3

    credentials = json_to_dict('resources/credentials/cre_pi.json')
    print(credentials)

    personality_insights = PersonalityInsightsV3(
        version=credentials['version'],
        iam_apikey=credentials['iam_apikey'],
        url=credentials['url']
    )

    return personality_insights


def analyse_data(personality_insights, path, prepared_path):
    from watson_developer_cloud.watson_service import WatsonApiException
    import os.path

    mk_dir(os.path.join(path, 'analysed'))

    _, name = os.path.split(prepared_path)

    analysed_json = os.path.join(path, 'analysed', name)

    try:
        with open(prepared_path) as profile_json:
            profile = personality_insights.profile(
                profile_json.read(),
                content_type='application/json',
                consumption_preferences=False,
                raw_scores=True
            ).get_result()
        dict_to_json(profile, analysed_json)

    except WatsonApiException as e:
        if e.code > 400:
            raise
        return 0

    return analysed_json


def condense(path, analysed_paths):
    """Condense raw Watson data to only important dayly info

    Args:
        path (String): path to user folder (ex. "users/georgy_k_zhukov/")
        prepared_path (String): path to raw Watson data (ex. "users/georgy_k_zhukov/prepared/1545253200.json")

    Returns:
        String: Path to condensed Watson data
    """
    import os.path
    from copy import deepcopy

    condensed = {}

    if not len(analysed_paths):
        raise Exception('Oopsie woopsie')

    mk_dir(os.path.join(path, 'condensed'))
    mk_dir(os.path.join(path, 'final'))

    average = {
        'Big_5': {
            "Openness"              : 0,
            "Conscientiousness"     : 0,
            "Extraversion"          : 0,
            "Agreeableness"         : 0,
            "Emotional range"       : 0
        },
        'Emotions': {
            "Fiery"                 : 0,
            "Prone to worry"        : 0,
            "Melancholy"            : 0,
            "Immoderation"          : 0,
            "Self-consciousness"    : 0,
            "Susceptible to stress" : 0
        }
    }


    data = {
        'Big_5': {
            "Openness"              : 0,
            "Conscientiousness"     : 0,
            "Extraversion"          : 0,
            "Agreeableness"         : 0,
            "Emotional range"       : 0
        },
        'Emotions': {
            "Fiery"                 : 0,
            "Prone to worry"        : 0,
            "Melancholy"            : 0,
            "Immoderation"          : 0,
            "Self-consciousness"    : 0,
            "Susceptible to stress" : 0
        }
    }

    for file in analysed_paths:
        data = {
            'Big_5': {
                "Openness"              : 0,
                "Conscientiousness"     : 0,
                "Extraversion"          : 0,
                "Agreeableness"         : 0,
                "Emotional range"       : 0
            },
            'Emotions': {
                "Fiery"                 : 0,
                "Prone to worry"        : 0,
                "Melancholy"            : 0,
                "Immoderation"          : 0,
                "Self-consciousness"    : 0,
                "Susceptible to stress" : 0
            }
        }

        date = os.path.split(file)[1].rstrip('.json')

        raw_data = json_to_dict(file)

        big_5 = raw_data['personality']

        for big in big_5:
            data['Big_5'][big['name']] = big['percentile']

            average['Big_5'][big['name']] += big['percentile']

            if big['name'] == 'Emotional range':
                emotions = big['children']
                for emotion in emotions:
                    data['Emotions'][emotion['name']] = emotion['percentile']

                    if emotion['percentile']:
                        average['Emotions'][emotion['name']] += emotion['percentile']

        condensed[date] = data.copy()

    compare = deepcopy(data)

    for key in average['Big_5'].keys():
        average['Big_5'][key] /= len(analysed_paths)

        if data['Big_5'][key] < average['Big_5'][key]:
            compare['Big_5'][key] = 'less'
        else:
            compare['Big_5'][key] = 'more'

    for key in average['Emotions'].keys():
        average['Emotions'][key] /= len(analysed_paths)

        if data['Emotions'][key] < average['Emotions'][key]:
            compare['Emotions'][key] = 'less'
        else:
            compare['Emotions'][key] = 'more'

    condensed_path = os.path.join(path, 'condensed', 'condensed.json')
    average_path   = os.path.join(path, 'final'    , 'average.json')
    last_path      = os.path.join(path, 'final'    , 'last.json')
    compare_path   = os.path.join(path, 'final'    , 'compared.json')

    dict_to_json(condensed, condensed_path)
    dict_to_json(average  , average_path)
    dict_to_json(data     , last_path)
    dict_to_json(compare  , compare_path)

    return condensed_path, compare_path


def get_watson_data(path, prepared_paths, prange, signal):
    progress = {
        'status'     : None,
        'error'      : None,
        'error_code' : None,
        'p'          : prange[0],
        'desc'       : None
    }

    delta = prange[1] - prange[0]

    try:
        progress['desc'] = 'Connecting to IBM Watson...'
        signal.emit(progress)
        personality_insights = watson_auth()
        progress['p']    = 0.1 * delta + prange[0]

        progress['desc'] = 'Analysing data with IBM Watson...'
        signal.emit(progress)
        analysed_paths = []
        a_delta = 0.9 * delta
        p = 0

        for prepared_path in prepared_paths:
            analysed_path = analyse_data(personality_insights, path, prepared_path)
            if analysed_path:
                analysed_paths.append(analysed_path)
            p += 1
            progress['p'] = round((p / len(prepared_paths)) * a_delta) + prange[0]
            signal.emit(progress)

        progress['desc'] = 'Processing analysed data...'
        signal.emit(progress)
        condensed_path = condense(path, analysed_paths)
        progress['p']    = 1.0 * delta + prange[0]

        progress['desc'] = 'Done!'
        signal.emit(progress)

        return condensed_path
    except Exception as e:
        raise
