from watson_developer_cloud import PersonalityInsightsV3
from os.path import join, dirname
import json

from utils import *

credentials = json_to_dict('credentials/cre_pi.json')

personality_insights = PersonalityInsightsV3(
    version=credentials['version'],
    iam_apikey=credentials['iam_apikey'],
    url=credentials['url']
)

with open(join(dirname(__file__), './profile.json')) as profile_json:
    profile = personality_insights.profile(
        profile_json.read(),
        content_type='application/json',
        consumption_preferences=True,
        raw_scores=True
    ).get_result()
print(json.dumps(profile, indent=2))
