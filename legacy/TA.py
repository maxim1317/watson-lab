from watson_developer_cloud import ToneAnalyzerV3
from watson_developer_cloud import WatsonApiException
import json

from utils import *


def analyse(text):
    credentials = json_to_dict('credentials/cre_ta.json')

    tone_analyzer = ToneAnalyzerV3(
        version=credentials['version'],
        iam_apikey=credentials['iam_apikey'],
        url=credentials['url']
    )

    try:
        tone_analysis = tone_analyzer.tone(
            {'text': text},
            'application/json'
        ).get_result()

        print(json.dumps(tone_analysis, indent=2))

        return tone_analysis

    except WatsonApiException as ex:
        print("Method failed with status code " + str(ex.code) + ": " + ex.message)

    return


def main():
    text = \
        'Team, I know that times are tough! Product '\
        'sales have been disappointing for the past three '\
        'quarters. We have a competitive product, but we '\
        'need to do a better job of selling it!'

    analyse(text)

    return
