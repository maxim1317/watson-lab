import os
import datetime
import os.path

from utils import *


def load_reddit(user_name, path):
    """Get user comments from reddit

    Args:
        user_name (String): reddit username
        path (String): path to user folder (ex.  "users/georgy_k_zhukov/")

    Returns:
        String: Path to raw reddit answers
    """
    import praw
    import time

    credentials = json_to_dict('credentials/cre_reddit.json')

    r = praw.Reddit(
        user_agent=credentials['my_user_agent'],
        client_id=credentials['my_client_id'],
        client_secret=credentials['my_client_secret']
    )

    mk_dir(os.path.join(path, 'raw'))

    user = r.redditor(user_name)

    user_comments = user.comments.new(limit=None)

    data = {}

    for comment in user_comments:
        base_time = datetime.datetime.min.time()
        d = datetime.datetime.combine(datetime.datetime.fromtimestamp(comment.created).date(), base_time)
        date = int(time.mktime(d.timetuple()))
        if date not in data:
            data[date] = {
                'comments': []
            }
        data[date]['comments'].append(comment.body)

    path = os.path.join(path, 'raw', user_name + '.json')

    dict_to_json(data, path)

    return path


def prepare_json(path, raw_path):
    """Prepare raw data to be sendable to Watson

    Args:
        path (String): path to user folder (ex. "users/georgy_k_zhukov/")
        raw_path (String): path to raw data JSON (ex. "users/georgy_k_zhukov/raw/georgy_k_zhukov.json")

    Returns:
        List: list of JSONs - data split by days and prepared for Watson. Each name is UNIX timestamp for the day
    """
    import time
    import os.path

    raw = json_to_dict(raw_path)

    mk_dir(os.path.join(path, 'prepared'))

    paths = []

    for date in raw.keys():
        now = datetime.datetime.now()
        wats = {
            'id'            : str(int(time.mktime(now.timetuple()))),
            'contenttype'   : 'text/plain',
            'created'       : date,
            'language'      : 'en',
            'content'         : ''
        }

        for comment in raw[date]['comments']:
            wats['content'] += comment + '.'

        output = {
            "contentItems": [wats]
        }

        out_path = os.path.join(path, 'prepared', date + '.json')

        dict_to_json(output, out_path)
        paths.append(out_path)

    return paths


def send_to_watson(path, prepared_path):
    """Send day data to Watson Personal Insights

    Args:
        path (String): path to user folder (ex. "users/georgy_k_zhukov/")
        prepared_path (String): path to prapared data (ex. "users/georgy_k_zhukov/prepared/1545253200.json")

    Returns:
        String: Path to raw Watson data
    """
    from watson_developer_cloud import PersonalityInsightsV3
    from watson_developer_cloud.watson_service import WatsonApiException

    credentials = json_to_dict('credentials/cre_pi.json')

    personality_insights = PersonalityInsightsV3(
        version=credentials['version'],
        iam_apikey=credentials['iam_apikey'],
        url=credentials['url']
    )

    mk_dir(os.path.join(path, 'analysed'))

    _, name = os.path.split(prepared_path)

    analysed_json = os.path.join(path, 'analysed', name)

    try:
        with open(prepared_path) as profile_json:
            profile = personality_insights.profile(
                profile_json.read(),
                content_type='application/json',
                consumption_preferences=False,
                raw_scores=True
            ).get_result()
        dict_to_json(profile, analysed_json)

    except WatsonApiException as e:
        print(e)
        return 0

    return analysed_json


def condense(path, analysed_paths):
    """Condense raw Watson data to only important dayly info

    Args:
        path (String): path to user folder (ex. "users/georgy_k_zhukov/")
        prepared_path (String): path to raw Watson data (ex. "users/georgy_k_zhukov/prepared/1545253200.json")

    Returns:
        String: Path to condensed Watson data
    """
    condensed = {}

    mk_dir(os.path.join(path, 'condensed'))

    for file in analysed_paths:
        data = {
            'Big_5': {
                "Openness"              : 0,
                "Conscientiousness"     : 0,
                "Extraversion"          : 0,
                "Agreeableness"         : 0,
                "Emotional range"       : 0
            },
            'Emotions': {
                "Fiery"                 : 0,
                "Prone to worry"        : 0,
                "Melancholy"            : 0,
                "Immoderation"          : 0,
                "Self-consciousness"    : 0,
                "Susceptible to stress" : 0
            }
        }

        date = os.path.split(file)[1].rstrip('.json')

        raw_data = json_to_dict(file)

        big_5 = raw_data['personality']

        for big in big_5:
            data['Big_5'][big['name']] = big['percentile']

            if big['name'] == 'Emotional range':
                emotions = big['children']
                for emotion in emotions:
                    data['Emotions'][emotion['name']] = emotion['percentile']

        condensed[date] = data.copy()

    out_path = os.path.join(path, 'condensed', 'condensed.json')

    dict_to_json(condensed, out_path)

    return out_path


def plot(path, mode='Big_5'):
    import matplotlib.pyplot as plt
    import matplotlib.dates as dates

    plt.xkcd()

    fig, ax = plt.subplots()

    plt.gca().xaxis.set_major_formatter(dates.DateFormatter('%Y-%m'))
    # plt.gca().xaxis.set_major_locator(dates.DayLocator())

    plt.gca().xaxis.set_major_locator(dates.MonthLocator())
    plt.gca().xaxis.set_minor_locator(dates.DayLocator())

    handles, labels = ax.get_legend_handles_labels()

    data = json_to_dict(path)

    x   = []
    y   = []
    y_1 = []
    y_2 = []
    y_3 = []
    y_4 = []
    y_5 = []
    y_6 = []

    if mode == 'Big_5':

        for date in data.keys():
            x.append(datetime.datetime.fromtimestamp(int(date)).date())
            y.append(data[date]["Big_5"])

        x, y = zip(*sorted(zip(x, y)))

        for yi in y:
            y_1.append(yi["Openness"])
            y_2.append(yi["Conscientiousness"])
            y_3.append(yi["Extraversion"])
            y_4.append(yi["Agreeableness"])
            y_5.append(yi["Emotional range"])

        l_1, = ax.plot(x, y_1, label="Openness")
        l_2, = ax.plot(x, y_2, label="Conscientiousness")
        l_3, = ax.plot(x, y_3, label="Extraversion")
        l_4, = ax.plot(x, y_4, label="Agreeableness")
        l_5, = ax.plot(x, y_5, label="Emotional range")

        plt.legend(handles=[l_1, l_2, l_3, l_4, l_5], loc=2)

    else:
        for date in data.keys():
            x.append(datetime.datetime.fromtimestamp(int(date)).date())
            y.append(data[date]["Emotions"])

        x, y = zip(*sorted(zip(x, y)))

        for yi in y:
            y_1.append(yi["Fiery"])
            y_2.append(yi["Prone to worry"])
            y_3.append(yi["Melancholy"])
            y_4.append(yi["Immoderation"])
            y_5.append(yi["Susceptible to stress"])
            y_6.append(yi["Self-consciousness"])

        l_1, = ax.plot(x, y_1, label="Fiery")
        l_2, = ax.plot(x, y_2, label="Prone to worry")
        l_3, = ax.plot(x, y_3, label="Melancholy")
        l_4, = ax.plot(x, y_4, label="Immoderation")
        l_5, = ax.plot(x, y_5, label="Susceptible to stress")
        l_6, = ax.plot(x, y_6, label="Self-consciousness")

        plt.legend(handles=[l_1, l_2, l_3, l_4, l_5, l_6], loc=2)

    fig.autofmt_xdate()

    # colors = ['r', 'g', 'b', 'y', 'c']

    plt.show()
    return


def main():
    user = 'georgy_k_zhukov'  # www.reddit.com/u/georgy_k_zhukov

    path = os.path.join('users', user)

    # raw_path = load_reddit(user, path)
    # prepared_paths = prepare_json(path, raw_path)

    # analysed_paths = []

    # for prepared in prepared_paths:
    #     analysed_path = send_to_watson(path, prepared)
    #     if analysed_path:
    #         analysed_paths.append(analysed_path)

    analysed = 'users/georgy_k_zhukov/analysed/'

    analysed_paths = [os.path.join(analysed, f) for f in os.listdir(analysed) if os.path.isfile(os.path.join(analysed, f))]

    condensed_path = condense(path, analysed_paths)

    plot(condensed_path, 'Big_5')


if __name__ == '__main__':
    main()
