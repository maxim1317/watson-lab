#!/bin/sh

if [ ! -f /tmp/foo.txt ]; then
    src/dontask.sh
else
    src/dontask_bk.sh
fi

(cd app && python3 main.py)